<a name="0.0.1"></a>
## [0.0.1](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v1.2.0...v0.0.1) (2016-01-21)


### Bug Fixes

* **transfer-behavior:** close alert only if it's visible ([b75c542](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/b75c542))
* **transfer-behavior.html:** Launched periodicity event ([34a3a4d](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/34a3a4d))

### Features

* **router-behavior:** allow params in setUrl function ([c35d404](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/c35d404))
* **router-behavior:** include navigation option to urlResolver ([bfd2020](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/bfd2020))



<a name="1.2.0"></a>
# [1.2.0](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v1.1.1...v1.2.0) (2016-01-15)




<a name="1.1.1"></a>
## [1.1.1](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v1.0.4...v1.1.1) (2016-01-11)


### Features

* **merge:** merge branch 'feature/locale-symbols' ([86cbe0e](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/86cbe0e))



<a name="1.0.4"></a>
## [1.0.4](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v1.0.3...v1.0.4) (2016-01-08)


### Bug Fixes

* **sort-behavior.html:** abstract function and condition to set strings to lowercase ([5a56e6e](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/5a56e6e))



<a name="1.0.3"></a>
## [1.0.3](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v1.1.0...v1.0.3) (2016-01-05)


### Features

* **router-behavior:** create router-behavior ([495cc63](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/495cc63))



<a name="1.1.0"></a>
# [1.1.0](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v1.0.2...v1.1.0) (2015-12-23)


### Bug Fixes

* **amount-behaviors.html:** delete cells-i18n-behavior ([85c97a2](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/85c97a2))

### Features

* **amount-behaviors.html:** include us symbols ([62a3141](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/62a3141))



<a name="1.0.2"></a>
## [1.0.2](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v1.0.1...v1.0.2) (2015-12-18)


### Features

* **html:** Added evento for sending info to users ([4325dd9](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/4325dd9))



<a name="1.0.1"></a>
## [1.0.1](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v1.0.0...v1.0.1) (2015-12-17)




<a name="1.0.0"></a>
# [1.0.0](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.2.1...v1.0.0) (2015-12-16)




<a name="0.2.1"></a>
## [0.2.1](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.2.0...0.2.1) (2015-12-15)




<a name="0.1.22"></a>
## [0.1.22](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.21...0.1.22) (2015-12-04)




<a name="0.1.21"></a>
## [0.1.21](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.20...0.1.21) (2015-12-03)


### Bug Fixes

* **i18n-behavior.html:** remove object observer ([207fd5e](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/207fd5e))



<a name="0.1.20"></a>
## [0.1.20](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.19...0.1.20) (2015-12-03)


### Bug Fixes

* use Polymer.dom() ([520c014](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/520c014))



<a name="0.1.19"></a>
## [0.1.19](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.18...0.1.19) (2015-12-03)


### Bug Fixes

* **i18n-behavior.html:** set cells-i18n component as hidden ([acc6a83](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/acc6a83))



<a name="0.1.18"></a>
## [0.1.18](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.17...0.1.18) (2015-12-02)


### Features

* **i18n-behavior.html:** add i18n behavior ([326b79a](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/326b79a))



<a name="0.1.17"></a>
## [0.1.17](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.16...0.1.17) (2015-12-02)


### Bug Fixes

* **cells-behaviors:** rename import ([93b0b01](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/93b0b01))



<a name="0.1.16"></a>
## [0.1.16](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.15...0.1.16) (2015-11-24)


### Features

* **cells-behaviors.html, cells-pubsub-behavior.html:** added bugfixed version of cells-pubsub-behavior ([b532fd4](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/b532fd4))



<a name="0.1.15"></a>
## [0.1.15](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.14...0.1.15) (2015-11-16)


### Features

* **responsive-behavior:** added check if Apache Cordova is available. ([22f2687](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/22f2687))



<a name="0.1.14"></a>
## [0.1.14](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.13...0.1.14) (2015-11-12)


### Bug Fixes

* **responsive-behavior.html:** fix script src import of device.js ([3211f8f](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/3211f8f))



<a name="0.1.13"></a>
## [0.1.13](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v0.1.12...0.1.13) (2015-11-11)




<a name="0.1.12"></a>
## [0.1.12](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.11...v0.1.12) (2015-11-04)




<a name="0.1.11"></a>
## [0.1.11](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.10...0.1.11) (2015-11-03)


### Bug Fixes

* **amount-behaviors.html:** properties well defined ([17609b6](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/17609b6))



<a name="0.1.10"></a>
## [0.1.10](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v0.1.9...v0.1.10) (2015-11-03)




<a name="0.1.9"></a>
## [0.1.9](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v0.1.8...v0.1.9) (2015-11-03)


### Features

* add iron-component-page to retrieve JsDocs ([884c82a](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/884c82a))
* remove and comment unused behaviors ([985e4a0](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/985e4a0))
* remove unused behavior ([a6736c9](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/a6736c9))



<a name="0.1.8"></a>
## [0.1.8](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.7...v0.1.8) (2015-10-27)


### Bug Fixes

* **js:** restore Cells var ([23222c0](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/23222c0))



<a name="0.1.7"></a>
## [0.1.7](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/v0.1.5...0.1.7) (2015-10-27)




<a name="0.1.5"></a>
## [0.1.5](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.6...v0.1.5) (2015-10-27)


### Bug Fixes

* **js:** add JSDocs meta to behavior ([e259213](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/e259213))



<a name="0.1.6"></a>
## [0.1.6](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.5...0.1.6) (2015-10-26)




<a name="0.1.5"></a>
## [0.1.5](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.4...0.1.5) (2015-10-26)




<a name="0.1.4"></a>
## [0.1.4](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.3...0.1.4) (2015-10-23)


### Features

* **amount-behaviors.html:** number formatting cross browser ([b46c7cc](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/b46c7cc))



<a name="0.1.3"></a>
## [0.1.3](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.2...0.1.3) (2015-10-19)


### Bug Fixes

* **amount-behaviors.html:** Amount format fixed ([3fd3ea3](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/3fd3ea3))

### Features

* **icons-behavior.html:** icon selection for movement details ([924199f](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/924199f))



<a name="0.1.2"></a>
## [0.1.2](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.1...0.1.2) (2015-09-30)




<a name="0.1.1"></a>
## [0.1.1](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/0.1.0...0.1.1) (2015-09-24)


### Bug Fixes

* **vulcanize:** ERROR finding ...cells-behaviors/sort-behavior.html ([41beca1](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/41beca1))



<a name="0.1.0"></a>
# [0.1.0](/https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/compare/4d11ae6...0.1.0) (2015-09-21)


### Features

* **amount-behaviors.html:** Added behaviors for filtering amount related tasks ([4d11ae6](https://descinet.bbva.es/stash/projects/CEL/reposlslabs/cells-ci-pull-all/commits/4d11ae6))



