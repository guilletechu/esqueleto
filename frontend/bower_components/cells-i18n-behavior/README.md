cells-i18n-behavior
=======================

`Polymer.i18nBehavior` provides a normalized interface for translate strings.

#### Import

1) Import the behavior in your component:

```html
<link rel="import" href="../cells-i18n-behavior/cells-i18n-behavior.html">
```

2) Add Polymer.i18nBehavior to the behaviors list in the JS file or script of your component:

```js
behaviors: [Polymer.i18nBehavior]
```

#### Usage

1. To translate a string, just do the following:

   ```html
   {{doTranslation('string_to_translate', lang)}}
   ```

   or, in a shorter but equivalent way (t() is just an alias of doTranslation()):

   ```html
   {{t('string_to_translate', lang)}}
   ```

2. Define an optional 'fallback' string with the second parameter:

   ```html
   {{t('string_to_translate', 'fallback_string', lang)}}
    ```

3. For asynchronous translation on attached:

   ```js
   attached: function() {
     this.getMsg().then(function() {
       this.readyTranslation = this.t('string_to_translate');
     }.bind(this));
  }
  ```


Where **string_to_translate** is the name of the string or message to be translated in your **locales/[en|es].json** files,
**fallback_string** is a message string to display if **string_to_translate** is not present in the dictionary, and
**lang** allows the behaviour to listen to dynamic language changes. Just add it as the second param of the function.

Note: If **string_to_translate** contains a comma (,) it must be escaped by preceding it with a '\'.

#### Example:

```html
<p class="buzz-ui-global-header__title">
 {{t('Welcome', lang)}}
 <input type="text" placeholder="{{t('Username\, Email or UserID', lang)}}">
</p>
```

Finally, have the translations on the *locales* folder. This folder will have one JSON file for each supported language (en.json, es.json, us.json, etc).

For every language the Object defined contains all translated strings indexed by a unique ID, which is the same across all languages.

***en.json:***

```json
{
 "your-component-name-cancel": {
   "message": "Cancel"
 },
 "your-component-name-info": {
   "message": "Information"
 }
}
```

***es.json:***

```json
{
 "your-component-name-cancel": {
   "message": "Cancelar"
 },
 "your-component-name-info": {
   "message": "Informacion"
 }
}
```
