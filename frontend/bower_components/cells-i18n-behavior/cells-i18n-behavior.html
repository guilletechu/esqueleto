<script>
/**
 * cells-i18n-behavior
 * =======================
 *
 * `Polymer.i18nBehavior` provides a normalized interface for translate strings.
 *
 * #### Import
 *
 * 1) Import the behavior in your component:
 *
 * ```html
 * <link rel="import" href="../cells-i18n-behavior/cells-i18n-behavior.html">
 * ```
 *
 * 2) Add Polymer.i18nBehavior to the behaviors list in the JS file or script of your component:
 *
 * ```js
 * behaviors: [Polymer.i18nBehavior]
 * ```
 *
 * #### Usage
 *
 * 1. To translate a string, just do the following:
 *
 *    ```html
 *    {{doTranslation('string-to-translate', lang)}}
 *    ```
 *
 *    or, in a shorter but equivalent way (t() is just an alias of doTranslation()):
 *
 *    ```html
 *    {{t('string-to-translate', lang)}}
 *    ```
 *
 * 2. Define an optional 'fallback' string with the second parameter:
 *
 *    ```html
 *    {{t('string-to-translate', 'fallback-string', lang)}}
 *     ```
 *
 * 3. For asynchronous translation on attached:
 *
 *    ```js
 *    attached: function() {
 *      this.getMsg().then(function() {
 *        this.readyTranslation = this.t('string-to-translate');
 *      }.bind(this));
 *    }
 *    ```
 *
 *
 * Where **string-to-translate** is the name of the string or message to be translated in your **locales/[en|es].json** files,
 * **fallback-string** is a message string to display if **string-to-translate** is not present in the dictionary, and
 * **lang** allows the behaviour to listen to dynamic language changes. Just add it as the second param of the function.
 *
 * Note: If **string-to-translate** contains a comma (,) it must be escaped by preceding it with a '\'.
 *
 * #### Example:
 *
 * ```html
 * <p class="buzz-ui-global-header__title">
 *   {{t('Welcome', lang)}}
 *   <input type="text" placeholder="{{t('Username\, Email or UserID', lang)}}">
 * </p>
 * ```
 *
 * Finally, have the translations on the *locales* folder. This folder will have one JSON file for each supported language (en.json, es.json, us.json, etc).
 *
 * For every language the Object defined contains all translated strings indexed by a unique ID, which is the same across all languages.
 *
 * ***en.json:***
 *
 *```json
 *{
 *  "your-component-name-cancel": {
 *    "message": "Cancel"
 *  },
 *  "your-component-name-info": {
 *    "message": "Information"
 *  }
 *}
 *```
 *
 * ***es.json:***
 *
 *```json
 *{
 *  "your-component-name-cancel": {
 *    "message": "Cancelar"
 *  },
 *  "your-component-name-info": {
 *    "message": "Informacion"
 *  }
 *}
 *```
 *
 * @demo demo/index.html
 * @polymerBehavior i18nBehavior
 */

Polymer.i18nBehavior = {

  /**
   * The `i18n-language-ready` is fired in `document` after the locale was fetched.
   *
   * @event i18n-language-ready
   * @detail {{language: String}}
   */

  properties: {
    /**
     * Current application language.
     */
    lang: {
      type: String,
      value: null,
      notify: true
    },

    /**
     * Puts a readonly attribute to the component to mark this node has i18n.
     * @private
     */
    hasI18n: {
      type: Boolean,
      value: true,
      readOnly: true,
      reflectToAttribute: true
    }
  },

  attached: function() {
    this.getMsg().then(this.updateTranslation.bind(this));
  },

  /**
   * Refresh the printed translated text with the current language.
   */
  updateTranslation: function() {
    this.lang = window.I18nMsg.lang;
  },

  /**
   * Translates a string to the current language.
   *
   * @param  {String} msgId String to translate.
   * @return {String} Translated text. If it doesn't exists, returns null.
   */
  getTranslation: function(msgId) {
    var locale = I18nMsg.currentLocale;

    msgId = msgId || this.msgid;

    if (msgId && locale && locale[msgId]) {
      return locale[msgId].message;
    }

    return null;
  },

  /**
   * Translates a string to the current language from html code.
   *
   * @param {String} msgId String id to translate.
   * @param {String} fallback (optional) A string to be returned if {{msgId}} doesn't exists in the dictionary. If this param is not defined and the translated text doesn't exists, the original {{msgId}} is returned.
   * @param {String} language (optional)
   *
   * @return {String} Translated text. If it doesn't exists, returns a fallback text.
   */
  doTranslation: function(msgId, fallback, language) {
    if (typeof language === 'undefined') {
      language = fallback;
      fallback = '';
    }

    return this.getTranslation(msgId) || fallback || msgId;
  },

  /**
   * doTranslation() alias
   */
  t: function(msgId, fallback, language) {
    return this.doTranslation(msgId, fallback, language);
  },

  /**
   * getAsyncTranslation() alias
   */
  getMsg: function(msgId) {
    return this.getAsyncTranslation(msgId);
  },

  /**
   * Translates a string when the dictionary is loaded.
   *
   * @param {String} msgId (optional) String id to translate.
   *
   * @return {Promise} Returns a Promise that is resolved when the dictionary is ready.
   */
  getAsyncTranslation: function(msgId) {
    var _this = this;
    return new Promise(function(resolve) {
      if (I18nMsg.currentLocale) {
        resolve(_this.getTranslation(msgId));
      } else {
        var langReadyAction = function() {
          document.removeEventListener('i18n-language-ready', langReadyAction);
          resolve(_this.getTranslation(msgId));
        };

        document.addEventListener('i18n-language-ready', langReadyAction);
      }
    });
  }
};

(function(window) {
  'use strict';

  window.I18nMsg = window.I18nMsg || {};

  var _xhr;
  var _url = window.I18nMsg.url || 'locales/';
  var _lang = window.I18nMsg.lang || document.documentElement.lang;
  var _locales = window.I18nMsg.locales || {};

  var _onLoadLanguage = function(e) {
    if (e.target.status !== 200 && e.target.status !== 0) {
      return;
    }

    I18nMsg.locales[I18nMsg.lang] = JSON.parse(e.target.response);

    _refreshNodes();
    _removeXHR();

    var event = new CustomEvent('i18n-language-ready', {language: I18nMsg.lang});
    document.dispatchEvent(event);
  };

  var _removeXHR = function(e) {
    _xhr = null;
  }

  var _fetchLanguage = function() {

    if (!I18nMsg.lang) {
      return;
    }

    if (I18nMsg.locales[I18nMsg.lang] && !_xhr) {
      _refreshNodes();
      return;
    }

    if (_xhr) {
      _xhr.abort();
    }

    var url = I18nMsg.url + I18nMsg.lang + '.json';

    _xhr = new XMLHttpRequest();
    _xhr.open('GET', url);
    _xhr.onload = _onLoadLanguage.bind(this);
    _xhr.onerror = _removeXHR.bind(this);
    _xhr.send();
  };

  var _refreshNodes = function() {
    //find components with cells-i18n-behavior
    var items = document.querySelectorAll('[has-i18n]');
    for (var i = items.length - 1; i >= 0; i--) {
      items[i].updateTranslation();
    }

    items = null;
  };

  I18nMsg.getCurrentLocale = function() {
    return this.currentLocale;
  };

  Object.defineProperty(I18nMsg, 'currentLocale', {
    get: function() {
      return _locales ? _locales[_lang] : false;
    }
  });

  Object.defineProperty(I18nMsg, 'locales', {
    set: function(val) {
      _locales = val;
    },
    get: function() {
      return _locales;
    }
  });

  Object.defineProperty(I18nMsg, 'lang', {
    set: function(val) {
      if (val !== _lang) {
        document.documentElement.lang = val;
        _lang = val;
        _fetchLanguage();
      }
    },
    get: function() {
      return _lang;
    }
  });

  Object.defineProperty(I18nMsg, 'url', {
    set: function(val) {
      // Make sure url ends on slash.
      if(val.substr(-1) !== '/') {
        val += '/';
      }

      if (val !== I18nMsg.url) {
        _url = val;
        _fetchLanguage();
      }
    },
    get: function() {
      return _url;
    }
  });
  window.I18nMsg.url = _url;
  window.I18nMsg.lang = _lang;
  _fetchLanguage();
})(window);
</script>
