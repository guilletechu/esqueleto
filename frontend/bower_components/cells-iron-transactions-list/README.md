![cells-iron-transactions-list screenshot](cells-iron-transactions-list.png)
# cells-iron-transactions-list

<!--[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)-->

[Demo to component in Cells Catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/elements/cells-iron-transactions-list)

`<cells-iron-transactions-list>` is a component that shows a list of transactions with `<iron-list>`.

This component has the component `cells-product-item` for show a transactions list. for his use, it´s neccessary to set the property transactions with data JSON.

```JSON
[{
  name: 'Amazon Product 1',
  description: {
    value: '0000123173568',
    masked: true,
    content: 'Cuenta Infinita'
  },
  primaryAmount: {
    amount: 123.32,
    currency: 'EUR'
  }
},
{
  name: 'Amazon Product 2',
  description: {
    value: '0000123103827',
    masked: true,
    content: 'Cuenta Infinita'
  },
  primaryAmount: {
    amount: 23.44,
    currency: 'EUR'
  }
}]
```

__Example with header:__

```html
<cells-iron-transactions-list header="transactions"></cells-iron-transactions-list>
```

It´s possible add other container or element to show scroll instead of `iron-list` with the properties `scrollTarget` and `scrollTargetType`.

In `scrollTarget` use the component that will have the scroll and in `scrollTargetType` set the type of selector `attribute | id | class`

__Example with scrollTarget:__

```html
<div class="target">
  <cells-iron-transactions-list scroll-target="target" scroll-target-type="class"></cells-iron-transactions-list>
</div>
```

__Slot content for errors and skeleton:__

It´s possible add your custom error using the slot for error messages and skeleton

```html
<cells-iron-transactions-list>
  <div slot="skeleton">
    <my-own-skeleton></my-own-skeleton>
  </div>
  <div slot="slot-error-message">
    <my-own-component-error></my-own-component-error>
  </div>
  <div slot="slot-empty-message">
    <my-own-component-error></my-own-component-error>
  </div>
</cells-iron-transactions-list>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property                                      | Description             | Default    |
|:-----------------------------------------------------|:------------------------|:----------:|
| --cells-iron-transactions-list                       | Mixin applied to :host  | {}         |
| --cells-iron-transactions-list-skeleton-bg-color     | Background color for skeleton   | #fff       |
| --cells-iron-transactions-list-skeleton-visible      | Extends --cells-skeleton-loading-page-visible  | #fff       |
| --cells-iron-transactions-list-skeleton-loading-page | Mixin applied to .skeleton  | {}       |
| --cells-fontDefault                                  | Font family of :host    | sans-serif |
| --cells-iron-transactions-list-error                 | mixin applied to error  |  |
| --cells-iron-transactions-list-messages              | mixin applied to the messages   |  |
| --cells-iron-transactions-list-empty                 | mixin applied to empty case  | |
