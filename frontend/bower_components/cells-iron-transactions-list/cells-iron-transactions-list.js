(function() {

  'use strict';

  Polymer({

    is: 'cells-iron-transactions-list',

    behaviors: [
      window.CellsBehaviors.i18nBehavior
    ],

    properties: {

      /**
       * Informative text when error:true
       */
      errorMessage: {
        type: String,
        value: 'cells-iron-transactions-list-error-message'
      },

      /**
       * Informative text when transactions come empty
       */
      emptyMessage: {
        type: String,
        value: 'cells-iron-transactions-list-empty'
      },

      /**
       * Header of the list
       */
      header: {
        type: String,
        value: null
      },

      /**
       * type in error message
       * @see cells-icon-message component
       */
      typeError: {
        type: String,
        value: 'error'
      },

      /**
       * icon in error message
       * @see cells-icon-message component
       */
      iconError: {
        type: String
      },

      /**
       * type in empty message
       * @see cells-icon-message component
       */
      typeEmpty: {
        type: String,
        value: 'warning'
      },

      /**
       * icon in empty message
       * @see cells-icon-message component
       */
      iconEmpty: {
        type: String
      },

      /**
       * Amount of px of width and height
       * of the icons of cells-icon-message component
       */
      iconsSize: {
        type: Number,
        value: 26
      },

      /**
       * Empty the transaction array
       */
      transactions: {
        type: Array
      },

      /**
       * Local currency code of the amounts
       */
      localCurrency: {
        type: String,
        value: 'CLP'
      },

      /**
       * Scale used to abbreviate the accounts amounts
       */
      scale: {
        type: Number
      },

      /**
       * Specifies the element that will handle the scroll event
       * on the behalf of the current element.
       */
      scrollTarget: {
        type: String
      },

      /**
       * A reference to parent element (attribute, class, id)
       */
      scrollTargetType: {
        type: String
      },

      _scrollTarget: {
        type: Object,
        computed: '_scrollTargetComputed(scrollTarget, scrollTargetType, isAttached)'
      },

      /**
       * Set some offset between the scrolling element and the list
       */
      scrollOffset: {
        type: Number,
        value: 0
      },

      _dataEmpty: {
        type: Boolean,
        value: false,
        computed: '_computeDataEmpty(transactions)'
      },

      /**
       * TODO: Remove and restructure HTML when the known-bug that makes
       * iron-list just work well the first time
       * inside of a dom-if is solved.
       */
      _ironListVisibility: {
        type: Boolean,
        computed: '_computeIronListVisibility(_dataEmpty, error, loading)'
      },

      /**
       * Indicates if component has no data
       */
      loading: {
        type: Boolean,
        value: false,
        notify: true,
        reflectToAttribute: true,
        observer: '_loadingChanged'
      },

      /**
       * Indicates if component has error data
       */
      error: {
        type: Boolean,
        value: false,
        notify: true,
        reflectToAttribute: true
      }

    },

    /**
     * Initializes component back to default state
     */
    reset: function(data) {
      if (data.value === false) {
        this.transactions = null;
        this.error = false;
        this.loading = true;
      }
    },

    /**
     * Component's data is loaded
     */
    onData: function() {
      this.error = false;
      this.loading = false;
    },

    /**
     * Component's data is not completed
     */
    onError: function() {
      this.transactions = null;
      this.error = true;
      this.loading = false;
    },

    /**
     * Be sure to use a number
     */
    _requiredNumber: function(scrollOffset) {
      var scrollOffsetNumber = parseInt(scrollOffset, 10);
      return isNaN(scrollOffsetNumber) ? 0 : scrollOffsetNumber;
    },

    _computeIronListVisibility: function(empty, error, loading) {
      this.$.ironList.notifyResize();
      return !empty && !error && !loading;
    },

    _computeDataEmpty: function(transactions) {
      var dataEmpty = false;

      if (transactions instanceof Array) {
        dataEmpty = transactions.length === 0;
      }

      return dataEmpty;
    },

    _scrollTargetComputed: function(scrollTarget, scrollTargetType, isAttached) {
      if (!isAttached || !scrollTargetType) {
        return;
      }

      /*eslint-disable */
      var scrollTargetElement = this; /*eslint-enable */

      var found = false;

      do {
        scrollTargetElement = scrollTargetElement.parentElement;
        switch (scrollTargetType) {
          case 'attribute':
            found = scrollTargetElement.hasAttribute(scrollTarget);
            break;
          case 'class':
            found = scrollTargetElement.classList.contains(scrollTarget);
            break;
          case 'id':
            found = scrollTargetElement.id === scrollTarget;
            break;
        }
      } while (!found && !!scrollTargetElement.parentElement);

      return scrollTargetElement;
    },

    _loadingChanged: function(loading) {
      if (loading) {
        this.$.loadingContainer.hidden = false;
        this.listen(this, 'transitionend', '_onTransitionEnd');
      }
    },

    /**
     * Fired when an item list is pressed
     * @event product-item-tap
     * @param {Object} detail { detail: e.model.transaction }
     */
    _onItemTap: function(e) {
      this.dispatchEvent(new CustomEvent('product-item-tap',
        { detail: e.model.transaction, bubbles: true, composed: true }
      ));
    },

    _onTransitionEnd: function(e) {
      this.unlisten(this, 'transitionend', '_onTransitionEnd');

      window.requestAnimationFrame(function() {
        this.$.loadingContainer.hidden = true;
      }.bind(this));
    }

  });

}());
