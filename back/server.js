var express = require('express'),
  cors = require('cors'),
  app = express(),
  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestJson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(cors());

var path = require('path');

// Conexión Mlab
var urlAcountsMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/acounts";
var urlTransactionsMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/transactions";
var apiKey = "apiKey=ncSplbUzyZHcoE-p5adUQc8XgOfaAcyZ";

//Conexión PostgreSQL en local
var pg = require('pg');
var urlUsuarios = "postgres://docker:docker@localhost:5433/bdseguridad";

//Conexión con búsqueda de Inbenta
var urlInbenta = "https://e03.inbenta.com/bbva_staging/?command=searchContents&question=";

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

/*********************************************
                  API Mlab
*********************************************/

app.get('/lastaccountmlab/:idusuario', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteAcountsMlab = requestJson.createClient(urlAcountsMlab + "?q={'idusuario':"
    + req.params.idusuario + "}&s={'id':-1}&f={'id':1}&l=1&" + apiKey);
    clienteAcountsMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/getaccounts', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteCuentasMlab = requestJson.createClient(urlAcountsMlab + "?q={'idusuario':"
    + req.body.idusuario + "}" + "&" + apiKey);
    clienteCuentasMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(err);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/accounts', function(req, res){
  clienteAcountsMlab = requestJson.createClient(urlAcountsMlab + "?" + apiKey);
  clienteAcountsMlab.post('',req.body, function(err, resM, body){
    if(err)
    {
      console.log(body);
      res.send(400);
    }
    else
    {
      res.send(200);
    }
  });
});

app.post('/gettransaction', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteCuentasMlab = requestJson.createClient(urlTransactionsMlab + "?q={'id':"
    + req.body.id + "}" + "&" + apiKey);
    clienteCuentasMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(err);
      }
      else
      {
        res.send(body);
      }
    });
});

/*********************************************
             API PostgreSQL
*********************************************/

app.post('/login', function(req, res){
    //Crear cliente PostgreSQL
    var clientePostgre = new pg.Client(urlUsuarios);
    clientePostgre.connect();
    const query = clientePostgre.query('SELECT idusuario FROM usuarios WHERE login =$1 AND password=$2;'
    , [req.body.login, req.body.password]
    , (err, result) => {
      if(err){
        console.log(err);
        res.send(err);
      }
      else{
        if(result.rows.length==1){
          res.send(result.rows[0].idusuario.toString());
        }
        else{
          res.send("-1");
        }
      }
    });
});

app.get('/userid/:login', function(req, res){
    //Crear cliente PostgreSQL
    var clientePostgre = new pg.Client(urlUsuarios);
    clientePostgre.connect();
    const query = clientePostgre.query('SELECT idusuario FROM usuarios WHERE login =$1;'
    , [req.params.login]
    , (err, result) => {
      if(err){
        console.log(err);
        res.send(err);
      }
      else{
        if(result.rows.length==1){
          res.send(result.rows[0].idusuario.toString());
        }
        else{
          res.send("-1");
        }
      }
    });
});

app.get('/userinfo/:idusuario', function(req, res){
    //Crear cliente PostgreSQL
    var clientePostgre = new pg.Client(urlUsuarios);
    clientePostgre.connect();
    const query = clientePostgre.query('SELECT login, nombre, apellido FROM usuarios WHERE idusuario =$1;'
    , [req.params.idusuario]
    , (err, result) => {
      if(err){
        console.log(err);
        res.send(err);
      }
      else{
        if(result.rows.length==1){
          var cliente={};
          cliente.nombre = result.rows[0].nombre;
          cliente.apellido = result.rows[0].apellido;
          res.send(cliente);
        }
        else{
          var cliente={};
          cliente.nombre = "invitado";
          cliente.apellido = "";
          res.send(cliente);
        }
      }
    });
});

app.post('/customer', function(req, res){
    //Crear cliente PostgreSQL
    var clientePostgre = new pg.Client(urlUsuarios);
    clientePostgre.connect();
    const query = clientePostgre.query('insert into usuarios (login, password, nombre, apellido) values ($1, $2, $3, $4);'
    , [req.body.login, req.body.password, req.body.nombre, req.body.apellido]
    , (err, result) => {
      if(err){
        console.log(err);
        res.send(422);
      }
      else{
        if(result.rowCount==1){
          res.send(200);
        }
        else{
          res.send(422);
        }
      }
    });
});

/*********************************************
                API Search
*********************************************/

app.get('/search/:searchfield', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteInbenta = requestJson.createClient(urlInbenta + req.params.searchfield);
    clienteInbenta.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});
